sysctlview 2.1
==============

**sysctlview** is a sysctl MIB explorer for [FreeBSD](http://www.freebsd.org),
each object represents a parameter of the kernel, **sysctlview** can show the
properties of an object and get or set its value. 
It depends on [gtkmm](https://www.gtkmm.org), 
[sysctlmibinfo2](https://gitlab.com/alfix/sysctlmibinfo2),
[sysctlinfo](https://gitlab.com/alfix/sysctlinfo) and
uses xdg-open (if
[devel/xdg-utils](https://www.freshports.org/devel/xdg-utils/)
is installed).  

To install the port
([deskutils/sysctlview](https://www.freshports.org/deskutils/sysctlview/)):

	# cd /usr/ports/deskutils/sysctlview/ && make install clean

To add the package:

	# pkg install sysctlview


Screenshots
-----------

Menu:  

![desktopMenu](screenshots/1_img.png)

Main Window:  

![values](screenshots/2_img.png)

Object Window:   

![description](screenshots/3_img.png)

Flags Window:   

![info](screenshots/4_img.png)

