/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2019-2024 Alfonso Sabato Siciliano
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <string.h>   // strlen
#include <inttypes.h> // string to num

#include <dev/evdev/input.h>  //struct input_id
#include <sys/resource.h>     //struct loadavg
#if defined(__amd64__) || defined(__i386__)
#include <machine/pc/bios.h>
#endif

#include <gtkmm/main.h>
#include <gtkmm/treeviewcolumn.h>

#include "model.h"
#include "progressbarwindow.h"
#include "sysctlexception.h"

struct ctl_type {
    const char *name;
    size_t size;
};

static const struct ctl_type ctl_types[CTLTYPE+1] = {
    { "ZEROUNUSED",       0                     },
    { "node",             0                     },
    { "integer",          sizeof(int)           },
    { "string",           0                     },
    { "int64_t",          sizeof(int64_t)       },
    { "opaque",           0                     },
    { "unsigned integer", sizeof(unsigned int)  },
    { "long integer",     sizeof(long int)      },
    { "unsigned long",    sizeof(unsigned long) },
    { "uint64_t",         sizeof(uint64_t)      },
    { "uint8_t",          sizeof(uint8_t)       },
    { "uint16_t",         sizeof(uint16_t)      },
    { "int8_t",           sizeof(int8_t)        },
    { "int16_t",          sizeof(int16_t)       },
    { "int32_t",          sizeof(int32_t)       },
    { "uint32_t",         sizeof(uint32_t)      }
};

/* Public Methods */

Model::Model()
{
    int i;

    m_Columns = new Gtk::TreeModel::ColumnRecord();

    m_Columns->add(m_col_isInternalRow);    
    m_Columns->add(m_col_name);

    for(i=0;i<NUM_COLSMAIN; i++)
	m_Columns->add(*(mainTreeCols[i].modelColumn));

    m_Columns->add(m_col_completename);
    m_Columns->add(m_col_uintflags);
    m_Columns->add(m_col_uinttype);

    for(i=0;i<NUM_COLSFLAGS; i++)
	m_Columns->add(*(flagTreeCols[i].modelColumn));

    //Create the Tree model
    m_refTreeModel = Gtk::TreeStore::create(*m_Columns);
    //Fill the TreeView's model
    //populateTreeModel(); mainwindow calls rebuildTrees()

    m_flagShowInternalRow = false;
    //Put the TreeModel inside a filter model:
    m_refTreeModelFilter = Gtk::TreeModelFilter::create(m_refTreeModel);
    m_refTreeModelFilter->set_visible_func(
	[this] (const Gtk::TreeModel::const_iterator& iter) -> bool
	{
	    //if(!iter)
	    //return true;

	    Gtk::TreeModel::Row row = *iter;
	    return (m_flagShowInternalRow || !row[m_col_isInternalRow]);
	});

    //Put the filter model inside a sort model:
    m_refTreeModelSort = Gtk::TreeModelSort::create(m_refTreeModelFilter);
    //m_refTreeModelSort->set_sort_column(m_col_name, Gtk::SORT_ASCENDING);
}

Model::~Model()
{
}

Glib::RefPtr<Gtk::TreeModelSort> Model::getRefTreeModelSort()
{
    return m_refTreeModelSort;
}

void Model::refreshValues()
{
    m_countValues=0;
    ProgressBarWindow *pbw = new ProgressBarWindow("Refreshing Values",true);
    while(Gtk::Main::events_pending())
	    Gtk::Main::iteration();

    m_refTreeModel->foreach_iter(sigc::mem_fun(*this,&Model::refreshValueIter));

    pbw->hide();
}

bool Model::rebuildTrees()
{
    bool res;

    m_refTreeModel->clear();
    res = populateTreeModel();
    m_refTreeModelFilter->refilter();
    
    return res;
}

void Model::toggleInternalRows()
{
    m_flagShowInternalRow = !m_flagShowInternalRow;
    m_refTreeModelFilter->refilter();
}

bool Model::hasHandler(Glib::ustring name)
{
    bool handler;

    if(sysctlmif_hashandlerbyname(name.c_str(), &handler) != 0)
	throw new SysctlException(errno);

    return handler;
}

Glib::ustring
Model::getSysctlValue(Glib::ustring name, bool withSkip, int longThreshold,
    Glib::ustring longstr)
{
    Glib::ustring value("");
    struct sysctlmif_object *obj;

    obj = sysctlmif_objectbyname(name.c_str());

    value += getSysctlValue(obj, withSkip, longThreshold, longstr);
    sysctlmif_freeobject(obj);

    return value;
}

Glib::ustring Model::setSysctlValue(Gtk::TreeModel::Row row, Glib::ustring newValue)
{
    size_t newval_size = 0, oidlevel = CTL_MAXNAME;
    const char *newvalstr = newValue.c_str();
    void *newval = NULL;
    int oid[CTL_MAXNAME], i;
    Glib::ustring name = row[m_col_completename];

    if(row[m_col_uinttype] == CTLTYPE_NODE)
	throw new SysctlException("A node object is not writable");

    if(row[m_col_TUN])
	throw new SysctlException("TUN flag: the value should be set in /boot/loader.conf");

    if(row[m_col_uinttype] == CTLTYPE_OPAQUE)
	throw new SysctlException("sysctlview can not set an OPAQUE value");

    if(row[m_col_RW] == false)
	throw new SysctlException("The value is not writable (the object has not the WR flag)");

    if(row[m_col_uinttype] != CTLTYPE_STRING && newValue.size() == 0)
	throw new SysctlException("Empty numeric new value");

    /* The value could be settable */

    if(row[m_col_uinttype] == CTLTYPE_STRING) {
	newval = (void*) newvalstr;
	newval_size = strlen(newvalstr) + 1;
    }
    // number value
    else
    {
	// some value is an array but fmt != A (e.g. kern.cp_times)
	gchar **splitstr = g_strsplit(newvalstr, ",", -1);
	for(i=0; splitstr[i] != NULL; i++){
	    newval_size += ctl_types[row[m_col_uinttype]].size;
	    newval = realloc(newval, ctl_types[row[m_col_uinttype]].size);
	    if(newval == NULL)
		throw new SysctlException("realloc() to create the numeric value");

	    switch (row[m_col_uinttype]) {
	    case CTLTYPE_INT:
		//if (strncmp(row[m_col_fmt], "IK", 2) == 0) {
		    //error += strIK_to_int(input, &kelvin, object->fmt);
		    //((int*)newval)[i] = kelvin;
		//} else {
		    ((int *)newval)[i] = (int)strtoll(splitstr[i], NULL, 0); 
		//}
		break;
	    case CTLTYPE_LONG:  
		((long *)newval)[i] = (long)strtoll(splitstr[i], NULL, 0);     
		break;
	    case CTLTYPE_S8:    
		((int8_t *)newval)[i] = (int8_t)strtoll(splitstr[i], NULL, 0);   
		break;
	    case CTLTYPE_S16:   
		((int16_t *)newval)[i] = (int16_t)strtoll(splitstr[i], NULL, 0);  
		break;
	    case CTLTYPE_S32:   
		((int32_t *)newval)[i] = (int32_t)strtoll(splitstr[i], NULL, 0);  
		break;
	    case CTLTYPE_S64:   
		((int64_t *)newval)[i] = (int64_t)strtoimax(splitstr[i], NULL, 0);  
		break;
	    case CTLTYPE_UINT:  
		((u_int *)newval)[i] = (u_int)strtoull(splitstr[i], NULL, 0);   
		break;
	    case CTLTYPE_ULONG: 
		((u_long *)newval)[i] = (u_long)strtoull(splitstr[i], NULL, 0);  
		break;
	    case CTLTYPE_U8:    
		((uint8_t *)newval)[i] = (uint8_t)strtoull(splitstr[i], NULL, 0); 
		break;
	    case CTLTYPE_U16:   
		((uint16_t *)newval)[i] = (uint16_t)strtoull(splitstr[i], NULL, 0);
		break;
	    case CTLTYPE_U32:   
		((uint32_t *)newval)[i] = (uint32_t)strtoull(splitstr[i], NULL, 0);
		break;
	    case CTLTYPE_U64:   
		((uint64_t *)newval)[i] = (uint64_t)strtoumax(splitstr[i], NULL, 0);
		break;
	    default:
		throw new SysctlException("Unknown Object type");
	    }// end switch
	    if(errno == EINVAL || errno == ERANGE) {
		throw new SysctlException("The new value is not a number");
	    }
	}// end for
	g_strfreev(splitstr); // XXX check memory leak after an exception
    } // else numeric value

    // XXX dont use sysctlbyname() for sysctl.name limitations
    if(sysctlmif_oidbyname(name.c_str(), oid, &oidlevel) != 0)
	throw new SysctlException("sysctlmibinfo2 cannot get the OID of the object");
    if(sysctl(oid, oidlevel, NULL, 0, newval, newval_size) != 0)
	throw new SysctlException(errno);

    // update tree model
    newValue = getSysctlValue(name.c_str(), true, -1, "");
    row[m_col_value] = newValue.size() <= 100 ? newValue : "[CLICK TO SHOW]";

    return newValue;
}

/* Private Methods */

bool Model::refreshValueIter(const Gtk::TreeModel::iterator& iter)
{
    if(iter)
    {
	Gtk::TreeModel::Row row = *iter;
	m_countValues++;
	if(m_countValues > 100) {
		m_countValues=0;
		while(Gtk::Main::events_pending())
		      Gtk::Main::iteration();
	}
	row[m_col_value] = getSysctlValue(row[m_col_completename], false, 100, 
	    Glib::ustring("[CLICK TO SHOW]"));
    }

    return false;
}

void Model::preorderAddNode(struct sysctlmif_object *node, Gtk::TreeModel::Row *parent)
{
    int i;
    char *name;
    Glib::ustring usValue(""), oidstr("");

    Gtk::TreeModel::Row row = parent != NULL
	? *(m_refTreeModel->append((*parent).children()))
	: *(m_refTreeModel->append());

    row[m_col_isInternalRow] = node->id[0] == 0;

    for(i=0; i< node->idlevel; i++) {
	if(i>0)
	    oidstr += ". ";
	oidstr += Glib::ustring::compose("%1", node->id[i]);
    }
    row[m_col_oid] = oidstr;

    row[m_col_completename] = Glib::ustring(node->name);

    for (i=strlen(node->name); i >= 0 ;i--)
	if(node->name[i] == '.')
	    break;
    name = &(node->name[i+1]);
    row[m_col_name] = Glib::ustring(name);

    if(node->desc != NULL)
	row[m_col_desc] = Glib::ustring(node->desc);

    if(node->label != NULL)
	row[m_col_label] = Glib::ustring(node->label);

    if(node->fmt != NULL)
	row[m_col_fmt] = Glib::ustring(node->fmt);

    row[m_col_type] = Glib::ustring(ctl_types[node->type].name);

    row[m_col_uinttype] = node->type;

    row[m_col_flags] = Glib::ustring::compose(
	"0x%1", Glib::ustring::format(std::hex, node->flags));

    row[m_col_uintflags] = node->flags;

    // flags (bools)
    for(i=0; i < NUM_COLSFLAGS; i++) {
	if ((node->flags & flagTreeCols[i].flag_bit) == flagTreeCols[i].flag_bit)
		row[*(flagTreeCols[i].modelColumn)] = 1;
	else
		row[*(flagTreeCols[i].modelColumn)] = 0;
    }

    // value
    row[m_col_value] = getSysctlValue(node, false, 100,
	Glib::ustring("[CLICK TO SHOW]"));

    struct sysctlmif_object *child;
    if(node->children != NULL && !SLIST_EMPTY(node->children))
	    SLIST_FOREACH(child, node->children, object_link)
		preorderAddNode(child,&row);
}

bool Model::populateTreeModel()
{
    struct sysctlmif_object *nodelevel1;
    struct sysctlmif_object_list *mib;

    if((mib = sysctlmif_mib()) == NULL) {
	return false;
    }

    ProgressBarWindow *pbw = new ProgressBarWindow("Building Tree",false);
    while(Gtk::Main::events_pending())
	    Gtk::Main::iteration();

    SLIST_FOREACH(nodelevel1, mib, object_link)
    {
	pbw->update(nodelevel1->name);
	while(Gtk::Main::events_pending())
		Gtk::Main::iteration();
	preorderAddNode(nodelevel1, NULL);
    }

    sysctlmif_freemib(mib);

    pbw->hide();

    return true;
}

Glib::ustring Model::getSysctlValue(struct sysctlmif_object * object,
    bool withSkip, int longThreshold, Glib::ustring longOrSkipStr)
{
    size_t valuesize = 0;
    void *value;
    Glib::ustring usValue("");
    int i, power10;
    float base;

    if(object->type == CTLTYPE_NODE)
	return usValue;

    if(!(object->flags & CTLFLAG_RD))
	return usValue;

    if(!withSkip && (object->flags & CTLFLAG_SKIP)) {
	usValue += longOrSkipStr;
	return usValue;
    }

    if(object->type == CTLTYPE_OPAQUE) {
	if(strcmp(object->fmt, "S,clockinfo") != 0 && 
	   strcmp(object->fmt, "S,input_id") != 0 &&
	   strcmp(object->fmt, "S,loadavg") != 0 &&
#if defined(__amd64__) || defined(__i386__)
	   strcmp(object->fmt, "S,bios_smap_xattr") != 0 &&
#endif
	   strcmp(object->fmt, "S,pagesizes") != 0 &&
	   strcmp(object->fmt, "S,timeval") != 0)
	    return usValue;
    }
    
    if(sysctl(object->id, object->idlevel, NULL, &valuesize, NULL, 0) !=0)
	return usValue;

    if(longThreshold > 0 && valuesize > longThreshold) {
	usValue += longOrSkipStr;
	return usValue;
    }

    /* valuesize could change after 2 calls of sysctl() */
    valuesize += valuesize;
    if((value = malloc(valuesize)) == NULL)
	return usValue;
    memset(value, 0, valuesize);

    if(sysctl(object->id, object->idlevel, value, &valuesize, NULL, 0) != 0)
    {
	// usValue = "";
    }
    else if(object->type == CTLTYPE_OPAQUE)
    {
	if(strcmp(object->fmt, "S,clockinfo") == 0)
	{
	    struct clockinfo *ci = (struct clockinfo*)value;
	    usValue += Glib::ustring::compose(
		"{ hz = %1, tick = %2, profhz = %3, stathz = %4 }", 
		ci->hz, ci->tick, ci->profhz, ci->stathz);
	}
#if defined(__amd64__) || defined(__i386__)
	else if(strcmp(object->fmt, "S,bios_smap_xattr") == 0)
	{
	    struct bios_smap_xattr *smap, *end;

	    end = (struct bios_smap_xattr *)((char *)value + valuesize);
	    for (smap = (struct bios_smap_xattr *)value; smap < end; smap++) {
		usValue += Glib::ustring::compose(
		    "\nSMAP type=%1, xattr=%2, base=%3, len=%4",
		    Glib::ustring::format(std::hex, smap->type),
		    Glib::ustring::format(std::hex, smap->xattr),
		    Glib::ustring::format(std::hex, (uintmax_t)smap->base),
		    Glib::ustring::format(std::hex, (uintmax_t)smap->length));
	    }
	}
#endif
	else if(strcmp(object->fmt, "S,input_id") == 0)
	{
	    struct input_id *id = (struct input_id*)value;
	    usValue += Glib::ustring::compose(
		"{ bustype = 0x%1, vendor = 0x%2, product = 0x%3, version = 0x%4 }",
		Glib::ustring::format(std::hex, id->bustype), 
		Glib::ustring::format(std::hex, id->vendor), 
		Glib::ustring::format(std::hex, id->product), 
		Glib::ustring::format(std::hex, id->version));
	}
	else if(strcmp(object->fmt, "S,loadavg") == 0)
	{
	    struct loadavg *tv = (struct loadavg*)value;
	    usValue += Glib::ustring::compose(
		"{ %1 %2 %3 }",
		Glib::ustring::format(std::hex, (double)tv->ldavg[0]/(double)tv->fscale),
		Glib::ustring::format(std::hex, (double)tv->ldavg[1]/(double)tv->fscale),
		Glib::ustring::format(std::hex, (double)tv->ldavg[2]/(double)tv->fscale));
	}
	else if(strcmp(object->fmt, "S,pagesizes") == 0)
	{
		usValue += "{ ";
		for (i = 0; i * sizeof(u_long) < valuesize && ((u_long*)value)[i] != 0; i++) {
		    if (i > 0)
			usValue += ", ";
		    usValue += Glib::ustring::compose("%1", ((unsigned long *)value)[i]);
		}
        	usValue += " }";
	}
	else if(strcmp(object->fmt, "S,timeval") == 0)
	{
	    struct timeval *tv = (struct timeval*)value;
	    time_t tv_sec;
	    char *p1;
	    usValue += Glib::ustring::compose(
	        "{ sec = %1, usec = %2 } ", (intmax_t)tv->tv_sec, tv->tv_usec);
	    tv_sec = tv->tv_sec;
	    p1 = strdup(ctime(&tv_sec));
	    p1[strlen(p1) -1] = '\0';
	    usValue += " ";
	    usValue += p1;
	    free(p1);
	}
    }
    else if(object->type == CTLTYPE_STRING)
    {
    	char *tmpstr = (char*)value;
	if (tmpstr[valuesize] != '\0')
	    tmpstr[valuesize] = '\0';
	if (tmpstr[strlen(tmpstr)-1] == '\n')
	    tmpstr[strlen(tmpstr)-1] = '\0';
	usValue += Glib::ustring((char*)value);
    }
#define GTVL(typevar) do {						\
        for (i=0; i < valuesize / sizeof( typevar); i++) {		\
            if (i > 0)							\
                usValue += " ";						\
            usValue += Glib::ustring::compose("%1", ((typevar *)value)[i]); \
        }								\
    } while(0)
    else if(object->type == CTLTYPE_INT)
	if (object->fmt[1] == 'K') {
	    power10 = 1;
	    if (object->fmt[2] != '\0')
		power10 = object->fmt[2] - '0';
	    base = 1.0;
	    for (i = 0; i < power10; i++)
		base *= 10.0;
	    usValue += Glib::ustring::compose("%1 C", *((int*)value) / base - 273.15);
	} else {
	    GTVL(int);
	}
    else if(object->type == CTLTYPE_S8)
	GTVL(int8_t);
    else if(object->type == CTLTYPE_S16)
	GTVL(int16_t);
    else if(object->type == CTLTYPE_S32)
	GTVL(int32_t);
    else if(object->type == CTLTYPE_LONG)
	GTVL(long);
    else if(object->type == CTLTYPE_S64)
	GTVL(int64_t);
    else if(object->type == CTLTYPE_UINT)
	GTVL(u_int);
    else if(object->type == CTLTYPE_U8)
	GTVL(uint8_t);
    else if(object->type == CTLTYPE_U16)
	GTVL(uint16_t);
    else if(object->type == CTLTYPE_U32)
	GTVL(uint32_t);
    else if(object->type == CTLTYPE_ULONG)
	GTVL(unsigned long);
    else if(object->type == CTLTYPE_U64)
	GTVL(uint64_t);
    else // opaque
	usValue += "";

    free(value);

    return usValue;
}
