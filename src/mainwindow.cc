/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2019-2024 Alfonso Sabato Siciliano
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <fstream>

#include <gtkmm/aboutdialog.h>
#include <gtkmm/box.h>
#include <gtkmm/checkmenuitem.h>
#include <gtkmm/label.h>
#include <gtkmm/menu.h>
#include <gtkmm/menubar.h>
#include <gtkmm/menuitem.h>
#include <gtkmm/messagedialog.h>
#include <gtkmm/separatormenuitem.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/stack.h>
#include <gtkmm/stackswitcher.h>
#include <gtkmm/treeview.h>
#include <gtkmm/treestore.h>

#include "mainwindow.h"

MainWindow::MainWindow()
{
    m_model = new Model();
    if(m_model->rebuildTrees() == false)
    	showRebuildTreesErrorDialog();

    Gtk::SearchEntry *searchEntry = new Gtk::SearchEntry();
    searchEntry->set_width_chars(50);
    m_trees = new Trees(this, searchEntry, m_model);
    Gtk::Box *box = new Gtk::Box(Gtk::ORIENTATION_VERTICAL, 10);
    int i;
    Gtk::CheckMenuItem *tmpItem;

    /* Main Window */
    set_title(PROGRAM_NAME);
    set_default_size(800, 600);
    set_border_width(10);

    /* icon */
    m_isIconSet=false;
    std::ifstream icon_port(ICON_PORT_PATH);
    if(icon_port.is_open()) {
	m_isIconSet=set_default_icon_from_file(ICON_PORT_PATH);
	icon_port.close();
    } else {
	std::ifstream icon_cwd(ICON_CWD_PATH);
	if(icon_cwd.is_open()) {
	    m_isIconSet=set_default_icon_from_file(ICON_CWD_PATH);
	    icon_cwd.close();
	}
    }

    /* Menu */
    // File
    Gtk::Menu *fileMenu = new Gtk::Menu();
    Gtk::MenuItem *fileItem = new Gtk::MenuItem("File");
    fileItem->set_submenu(*fileMenu);

    Gtk::MenuItem *refreshItem = new Gtk::MenuItem("Refresh values");
    refreshItem->signal_activate().connect(
	sigc::mem_fun(*this, &MainWindow::on_refreshMenuItem));
    fileMenu->append(*refreshItem);

    Gtk::MenuItem *rebuildItem = new Gtk::MenuItem("Rebuild all");
    rebuildItem->signal_activate().connect(
	sigc::mem_fun(*this, &MainWindow::on_rebuildMenuItem));
    fileMenu->append(*rebuildItem);

    fileMenu->append(*(new Gtk::SeparatorMenuItem()));

    Gtk::MenuItem *quitItem = new Gtk::MenuItem("Quit");
    quitItem->signal_activate().connect(
	sigc::mem_fun(*this, &MainWindow::on_quitMenuItem));

    fileMenu->append(*quitItem);

    // View
    Gtk::Menu *viewMenu = new Gtk::Menu();
    Gtk::MenuItem *viewItem = new Gtk::MenuItem("View");
    viewItem->set_submenu(*viewMenu);

    Gtk::MenuItem *expandsItem = new Gtk::MenuItem("Expand all nodes ");
    expandsItem->signal_activate().connect(
	sigc::mem_fun(*this, &MainWindow::on_expandsMenuItem));
    viewMenu->append(*expandsItem);

    Gtk::MenuItem *collapseItem = new Gtk::MenuItem("Collapse the tree");
    collapseItem->signal_activate().connect(
	sigc::mem_fun(*this, &MainWindow::on_collapseMenuItem));
    viewMenu->append(*collapseItem);

    viewMenu->append(*(new Gtk::SeparatorMenuItem()));
    Gtk::CheckMenuItem *InternalItem = new Gtk::CheckMenuItem("Internal nodes");
    InternalItem->signal_activate().connect(
	sigc::mem_fun(*this, &MainWindow::on_sysctlMenuItem));
    viewMenu->append(*InternalItem);

    Gtk::CheckMenuItem *searchBarItem = new Gtk::CheckMenuItem("Search Name");
    searchBarItem->signal_activate().connect(
	sigc::mem_fun(*this, &MainWindow::on_searchBarMenuItem));
    viewMenu->append(*searchBarItem);

    Gtk::CheckMenuItem *wrapItem = new Gtk::CheckMenuItem("Wrap rows");
    wrapItem->set_active(true);
    wrapItem->signal_activate().connect(
	sigc::mem_fun(*this, &MainWindow::on_wrapMenuItem));
    viewMenu->append(*wrapItem);

    viewMenu->append(*(new Gtk::SeparatorMenuItem()));

    // View -> Main tree
    Gtk::MenuItem *mainsubmenuItem = new Gtk::MenuItem("Main Tree");
    viewMenu->append(*mainsubmenuItem);
    Gtk::Menu *mainMenu = new Gtk::Menu();
    mainsubmenuItem->set_submenu(*mainMenu);

    Gtk::MenuItem *allMainItem = new Gtk::MenuItem("All");
    allMainItem->signal_activate().connect(
	sigc::mem_fun(*this, &MainWindow::on_allCheckMain));
    mainMenu->append(*allMainItem);

    Gtk::MenuItem *defaultMainItem = new Gtk::MenuItem("Defaults");
    defaultMainItem->signal_activate().connect(
	sigc::mem_fun(*this, &MainWindow::on_defaultsCheckMain));
    mainMenu->append(*defaultMainItem);

    mainMenu->append(*(new Gtk::SeparatorMenuItem()));

    for(i=0;i<NUM_COLSMAIN; i++) {
	tmpItem = new Gtk::CheckMenuItem(m_model->mainTreeCols[i].infoName);
	tmpItem->set_active(m_model->mainTreeCols[i].visible);
	tmpItem->signal_activate().connect( 
	    sigc::bind<int>( 
		sigc::mem_fun(*this, &MainWindow::on_viewMainTreeColumn), i+1));
	m_listCheckMain[i]=tmpItem;
	mainMenu->append(*tmpItem);
    }
    // View -> Flags tree
    Gtk::MenuItem *flagssubmenuItem = new Gtk::MenuItem("Flags Tree");
    viewMenu->append(*flagssubmenuItem);
    Gtk::Menu *flagsMenu = new Gtk::Menu();
    flagssubmenuItem->set_submenu(*flagsMenu);

    Gtk::MenuItem *allFlagsItem = new Gtk::MenuItem("All");
    allFlagsItem->signal_activate().connect(
	sigc::mem_fun(*this, &MainWindow::on_allCheckFlags));
    flagsMenu->append(*allFlagsItem);

    Gtk::MenuItem *defaultFlagsItem = new Gtk::MenuItem("Defaults");
    defaultFlagsItem->signal_activate().connect(
	sigc::mem_fun(*this, &MainWindow::on_defaultsCheckFlags));
    flagsMenu->append(*defaultFlagsItem);

    flagsMenu->append(*(new Gtk::SeparatorMenuItem()));

    for(i=0;i<NUM_COLSFLAGS; i++) {
	tmpItem = new Gtk::CheckMenuItem(m_model->flagTreeCols[i].flag_name);
	tmpItem->set_active(m_model->flagTreeCols[i].visible);
	tmpItem->signal_activate().connect( 
	    sigc::bind<int>( 
		sigc::mem_fun(*this, &MainWindow::on_viewFlagsTreeColumn),i+1));
	m_listCheckFlags[i]=tmpItem;
	flagsMenu->append(*tmpItem);
    }

    // Help
    Gtk::Menu *helpMenu = new Gtk::Menu();
    Gtk::MenuItem *helpItem = new Gtk::MenuItem("Help");
    helpItem->set_submenu(*helpMenu);

    Gtk::MenuItem *flagsItem = new Gtk::MenuItem("Flags");
    flagsItem->signal_activate().connect(
	sigc::mem_fun(*this, &MainWindow::on_flagsMenuItem));
    helpMenu->append(*flagsItem);

    Gtk::MenuItem *handbookItem = new Gtk::MenuItem("Handbook");
    handbookItem->signal_activate().connect(
	sigc::mem_fun(*this, &MainWindow::on_handbookMenuItem));
    helpMenu->append(*handbookItem);

    Gtk::MenuItem *faqItem = new Gtk::MenuItem("FAQ");
    faqItem->signal_activate().connect(
	sigc::mem_fun(*this, &MainWindow::on_faqMenuItem));
    helpMenu->append(*faqItem);

    Gtk::MenuItem *aboutItem = new Gtk::MenuItem("About");
    aboutItem->signal_activate().connect(
	sigc::mem_fun(*this, &MainWindow::on_aboutMenuItem));
    helpMenu->append(*aboutItem);

    Gtk::MenuBar *menubar = new Gtk::MenuBar();
    menubar->append(*fileItem);
    menubar->append(*viewItem);
    menubar->append(*helpItem);
    box->add(*menubar);

    /* StackSwitcher and Stack */
    Gtk::Stack *stack = new Gtk::Stack();

    Gtk::ScrolledWindow *scrolledwindow1 = new Gtk::ScrolledWindow();
    Gtk::TreeView *m_mainTreeView = m_trees->getMainTree();
    scrolledwindow1->add((*m_mainTreeView));
    //Show scrollbars only when they are necessary:
    scrolledwindow1->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
    scrolledwindow1->set_propagate_natural_width(true);
    scrolledwindow1->set_propagate_natural_height(true);
    stack->add(*scrolledwindow1,"mainStack", "Main");

    Gtk::ScrolledWindow *scrolledwindow2 = new Gtk::ScrolledWindow();
    Gtk::TreeView *flagsTreeView = m_trees->getFlagsTree();
    scrolledwindow2->add((*flagsTreeView));
    scrolledwindow2->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
    scrolledwindow2->set_propagate_natural_width(true);
    scrolledwindow2->set_propagate_natural_height(true);
    stack->add(*scrolledwindow2,"flagsStack", "Flags");

    Gtk::StackSwitcher *stackswitcher = new Gtk::StackSwitcher();
    stackswitcher->set_stack(*stack);
    //Gtk::HeaderBar *headerbar = new Gtk::HeaderBar();
    //headerbar->add(*stackswitcher);
    //box->add(*headerbar);    
    box->add(*stackswitcher);


    //searchEntry = new Gtk::SearchEntry();
    //searchEntry->signal_search_changed().connect(
    //	sigc::mem_fun(*this, &MainWindow::on_search_text_changed));
    m_searchBar = new Gtk::SearchBar();
    //m_searchBar->connect_entry(*m_searchEntry);
    m_searchBar->set_search_mode(false);
    m_searchBar->set_show_close_button(false);
    m_searchBar->add(*searchEntry);
    box->add(*m_searchBar);


    box->add(*stack);


    /* Add box to the m_window */
    add(*box);
    show_all_children();
}

MainWindow::~MainWindow()
{
}

/* Private methods */

void MainWindow::showRebuildTreesErrorDialog()
{
    Glib::ustring text2("");
    Gtk::MessageDialog sysctlinfowarn(*this, "Error getting MIB info", false,
	Gtk:: MESSAGE_ERROR);

    text2 += "Make sure that you have loaded the sysctlinfo kernel module, ";
    text2 += "by doing\n\n";
    text2 += "     # kldload sysctlinfo\n\n";
    text2 += "or adding\n\n";
    text2 += "     sysctlinfo_load=\"YES\"\n\n";
    text2 += "to your /boot/loader.conf.\n";

    sysctlinfowarn.set_secondary_text(text2);
    sysctlinfowarn.run();
}

void MainWindow::on_refreshMenuItem()
{
    m_model->refreshValues();
}

void MainWindow::on_rebuildMenuItem()
{
    if(m_model->rebuildTrees() == false)
	showRebuildTreesErrorDialog();
    
    m_trees->autoResizeColumns();
}

void MainWindow::on_quitMenuItem()
{
    this->hide();
}

void MainWindow::on_expandsMenuItem()
{
    m_trees->expandTrees();
}

void MainWindow::on_collapseMenuItem()
{
    m_trees->collapseTrees();
    m_trees->autoResizeColumns();
}

void MainWindow::on_searchBarMenuItem()
{
    m_searchBar->set_search_mode(!m_searchBar->get_search_mode());
}

void MainWindow::on_wrapMenuItem()
{
    m_trees->toggleWrapRows();
}

void MainWindow::on_sysctlMenuItem()
{
    m_model->toggleInternalRows();
}

void MainWindow::on_allCheckMain()
{
    int i;

    for(i=0; i< NUM_COLSMAIN; i++)
	m_listCheckMain[i]->set_active(true);
}

void MainWindow::on_defaultsCheckMain()
{
    int i;

    for(i=0; i< NUM_COLSMAIN; i++)
	m_listCheckMain[i]->set_active(m_model->mainTreeCols[i].visible);
}

void MainWindow::on_allCheckFlags()
{
    int i;

    for(i=0; i< NUM_COLSFLAGS; i++)
	m_listCheckFlags[i]->set_active(true);
}

void MainWindow::on_defaultsCheckFlags()
{
    int i;

    for(i=0; i< NUM_COLSFLAGS; i++)
	m_listCheckFlags[i]->set_active(m_model->flagTreeCols[i].visible);
}

void MainWindow::on_viewMainTreeColumn(int col)
{
    m_trees->toggleMainTreeColumn(col);
}

void MainWindow::on_viewFlagsTreeColumn(int col)
{
    m_trees->toggleFlagsTreeColumn(col);
}

void MainWindow::on_flagsMenuItem()
{
    Glib::ustring line("");
    int i;
    Gtk::MessageDialog dialog(*this, "Flags");

    for(i=0;i<NUM_COLSFLAGS; i++) {
	line += Glib::ustring::sprintf("\n   [%s]:    %s",
	    m_model->flagTreeCols[i].flag_name, m_model->flagTreeCols[i].flag_desc);
    }

    dialog.set_secondary_text(line);
    dialog.run();
}

void MainWindow::on_faqMenuItem()
{
    Glib::ustring faq("");
    Gtk::MessageDialog dialog(*this, "FAQ");

    faq += "What opaque formats are available?\n";
    faq += "   clockinfo, input_id, loadavg, pagesizes and timeval.\n\n";
    faq += "Does exist a terminal utility to show internal nodes, OID, ";
    faq += "format, label, flags and handler?\n";
    faq += "   You could install sysutils/nsysctl.\n";
    dialog.set_secondary_text(faq);
    dialog.run();
}

void MainWindow::on_aboutMenuItem()
{
    Gtk::AboutDialog m_Dialog;
    Glib::ustring license("");

    m_Dialog.set_transient_for(*this);
    if(m_isIconSet)
	m_Dialog.set_logo_default();
    m_Dialog.set_program_name(PROGRAM_NAME);
    m_Dialog.set_version("version " PROGRAM_VERSION);
    m_Dialog.set_comments("sysctl MIB explorer");
    m_Dialog.set_website("https://gitlab.com/alfix/sysctlview");
    m_Dialog.set_website_label("gitlab.com/alfix/sysctlview");
    m_Dialog.set_copyright(COPYRIGHTSTRING);

#define LICENSE \
"Redistribution and use in source and binary forms, with or\n"\
"without modification, are permitted provided that the\n"\
"following conditions are met:\n"\
"\n"\
"1. Redistributions of source code must retain the above\n"\
"   copyright notice, this list of conditions and the\n"\
"   following disclaimer.\n"\
"2. Redistributions in binary form must reproduce the\n"\
"   above copyright notice, this list of conditions and\n"\
"   the following disclaimer in the documentation and/or\n"\
"   other materials provided with the distribution.\n"\
"\n"\
"THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND\n"\
"CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED\n"\
"WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED\n"\
"WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A\n"\
"PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL\n"\
"THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,\n"\
"INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR\n"\
"CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,\n"\
"PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF\n"\
"USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)\n"\
"HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,\n"\
" WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT\n"\
"(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY\n"\
"WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED\n"\
"OF THE POSSIBILITY OF SUCH DAMAGE.\n"

    m_Dialog.set_license(COPYRIGHTSTRING "\n\n" LICENSE);

    std::vector<Glib::ustring> list_authors;
    list_authors.push_back("Alfonso Sabato Siciliano");
    list_authors.push_back("https://alfonsosiciliano.gitlab.io");
    list_authors.push_back("alf.siciliano@gmail.com");
    m_Dialog.set_authors(list_authors);

    m_Dialog.run();
}
void MainWindow::on_handbookMenuItem()
{   
    gchar *tool = g_find_program_in_path("xdg-open");
    gchar *command;
    Glib::ustring url(""), errorStr("");
    
    url = "https://docs.freebsd.org/en/books/handbook/config/#configtuning-sysctl";

    if(tool == NULL) {
	Gtk::MessageDialog xdgError(*this, "Warning", false, Gtk::MESSAGE_WARNING);
	errorStr = "\nsysctlview cannot open the browser\n\n" + url;
	xdgError.set_secondary_text(errorStr);
	xdgError.run();
    }
    else {
	command = g_strconcat(tool, " ", url.c_str(), NULL);
        g_spawn_command_line_async(command, NULL);
        g_free(tool);
        g_free(command);
    }

}
