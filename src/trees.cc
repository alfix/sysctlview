/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2019-2024 Alfonso Sabato Siciliano
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <string.h>
#include <gtkmm/cellrenderertext.h>
#include <gtkmm/messagedialog.h>
#include <gtkmm/treeviewcolumn.h>
#include <gdkmm/rgba.h>
#include <gtkmm/stylecontext.h>

#include "trees.h"
#include "objectwindow.h"

/* Public Methods */

Trees::Trees(Gtk::Window *w, Gtk::SearchEntry *searchEntry, Model *m)
{
    int i;
    Gtk::TreeViewColumn* tmpColumn;

    m_Window = w;
    m_model = m;

    m_mainTree = new Gtk::TreeView();
    m_flagsTree = new Gtk::TreeView();

    // m_mainTree
    m_mainTree->set_model(m_model->getRefTreeModelSort());

    tmpColumn = new Gtk::TreeViewColumn("Name", m_model->m_col_name);
    setColumnColorIfBlack(tmpColumn, "blue", "yellow");
    tmpColumn->set_min_width(50);
    tmpColumn->set_resizable(true);
    m_mainTree->append_column(*tmpColumn);
    tmpColumn->set_sort_column(m_model->m_col_name);

    m_mainTree->set_enable_search(true);
    m_mainTree->set_search_column(0);
    m_mainTree->set_search_entry(*searchEntry);
    m_mainTree->set_search_equal_func(sigc::mem_fun(*this,&Trees::on_search_equal));

    for(i=0;i<NUM_COLSMAIN; i++) {
	tmpColumn = new Gtk::TreeViewColumn(m_model->mainTreeCols[i].infoName, 
					    *(m_model->mainTreeCols[i].modelColumn));
	tmpColumn->set_min_width(50);
	tmpColumn->set_resizable(true);
	tmpColumn->set_visible(m_model->mainTreeCols[i].visible);
	
	Gtk::CellRendererText* pRenderer =
	    static_cast<Gtk::CellRendererText*>(tmpColumn->get_first_cell());
	pRenderer->property_wrap_width() = WRAP_SIZE_COLS;
	pRenderer->property_wrap_mode() = Pango::WRAP_WORD_CHAR;

	m_mainTree->append_column(*tmpColumn);
	tmpColumn->set_sort_column(*(m_model->mainTreeCols[i].modelColumn));
    }

    m_mainTree->set_enable_tree_lines(true);
    m_mainTree->set_grid_lines(Gtk::TREE_VIEW_GRID_LINES_BOTH);
    //Reordered with drag-and-drop:
    //m_mainTree->set_reorderable();
    
    //Connect signal:
    m_mainTree->signal_row_activated().connect(
	sigc::mem_fun(*this, &Trees::on_rowActivated));

    // m_flagsTree
    m_flagsTree->set_model(m_model->getRefTreeModelSort());

    tmpColumn = new Gtk::TreeViewColumn("Name", m_model->m_col_name);
    setColumnColorIfBlack(tmpColumn, "blue", "yellow");
    tmpColumn->set_min_width(50);
    tmpColumn->set_resizable(true);
    m_flagsTree->append_column(*tmpColumn);
    tmpColumn->set_sort_column(m_model->m_col_name);

    m_flagsTree->set_enable_search(true);
    m_flagsTree->set_search_column(0);
    m_flagsTree->set_search_entry(*searchEntry);
    m_flagsTree->set_search_equal_func(sigc::mem_fun(*this,&Trees::on_search_equal));

    for(i=0;i<NUM_COLSFLAGS; i++) {
	tmpColumn = new Gtk::TreeViewColumn(m_model->flagTreeCols[i].flag_name, 
	    *(m_model->flagTreeCols[i].modelColumn));
	tmpColumn->set_min_width(50);
	tmpColumn->set_resizable(true);
	tmpColumn->set_visible(m_model->flagTreeCols[i].visible);
	m_flagsTree->append_column(*tmpColumn);
	tmpColumn->set_sort_column(*(m_model->flagTreeCols[i].modelColumn));
    }

    m_flagsTree->set_enable_tree_lines(true);
    m_flagsTree->set_grid_lines(Gtk::TREE_VIEW_GRID_LINES_BOTH);
        
    //Connect signal:
    m_flagsTree->signal_row_activated().connect(
	sigc::mem_fun(*this, &Trees::on_rowActivated) );
}

Trees::~Trees()
{
}

Gtk::TreeView * Trees::getMainTree()
{
    return m_mainTree;
}

Gtk::TreeView * Trees::getFlagsTree()
{
    return m_flagsTree;
}

void Trees::expandTrees()
{
    m_mainTree->expand_all();
    m_flagsTree->expand_all();
}

void Trees::collapseTrees()
{
    m_mainTree->collapse_all();
    m_flagsTree->collapse_all();
}

void Trees::toggleWrapRows()
{
    int i, num_cols;
    Gtk::CellRendererText* pRenderer;

    pRenderer = static_cast<Gtk::CellRendererText*>(
	m_mainTree->get_column(0)->get_first_cell());
    if(pRenderer->property_wrap_width() > -1)
	pRenderer->property_wrap_width() = -1;
    else
	pRenderer->property_wrap_width() = WRAP_SIZE_NAME_COL;

    num_cols=m_mainTree->get_n_columns();
    // 0 is the 'name' column
    for(i=1; i < num_cols; i++) {
	pRenderer = static_cast<Gtk::CellRendererText*>(
	    m_mainTree->get_column(i)->get_first_cell());
	if(pRenderer->property_wrap_width() > -1)
	    pRenderer->property_wrap_width() = -1;
	else
	    pRenderer->property_wrap_width() = WRAP_SIZE_COLS;
    }

    pRenderer = static_cast<Gtk::CellRendererText*>(
	m_flagsTree->get_column(0)->get_first_cell());
    if(pRenderer->property_wrap_width() > -1)
	pRenderer->property_wrap_width() = -1;
    else
	pRenderer->property_wrap_width() = WRAP_SIZE_NAME_COL;

    autoResizeColumns();
}

void Trees::toggleMainTreeColumn(int col)
{
    bool isVisible = m_mainTree->get_column(col)->get_visible();
    m_mainTree->get_column(col)->set_visible(!isVisible);
}

void Trees::toggleFlagsTreeColumn(int col)
{
    bool isVisible = m_flagsTree->get_column(col)->get_visible();
    m_flagsTree->get_column(col)->set_visible(!isVisible);
}

void Trees::autoResizeColumns()
{
    m_mainTree->columns_autosize();
    m_flagsTree->columns_autosize();
}


/* Private Methods */

bool
Trees::on_search_equal(const Glib::RefPtr<Gtk::TreeModel>& model, int column, 
    const Glib::ustring& key, const Gtk::TreeModel::iterator& iter)
{
     if(iter)
     {
	     Gtk::TreeModel::Row row = *iter;
	     Glib::ustring name(row[m_model->m_col_completename]);
	     if(strncmp(name.c_str(), key.c_str(), name.size() ) == 0) {
	          m_mainTree->expand_to_path(Gtk::TreePath(iter));
		  m_flagsTree->expand_to_path(Gtk::TreePath(iter));
	     }
	     if(strncmp(key.c_str(), name.c_str(), key.size() ) == 0)
	          return false;
     }
     
     return true;
}

void
Trees::setColumnColorIfBlack(Gtk::TreeViewColumn *tvc, Glib::ustring colorLight,
    Glib::ustring colorDark)
{
    Glib::RefPtr<Gtk::StyleContext> style = m_mainTree->get_style_context();
    Gdk::RGBA foreground = style->get_color();
    Gtk::CellRendererText* pText = static_cast<Gtk::CellRendererText*>(tvc->get_first_cell());

    /* extra check && foreground.get_alpha() == 0 */
    if(foreground.get_red() < 0.2 && foreground.get_green() < 0.2 && foreground.get_blue() < 0.2)
	pText->property_foreground() = colorLight;
	//pText->property_foreground_rgba() = Gdk::RGBA("rgb(10,70,5)");

    if(foreground.get_red() ==1 && foreground.get_green() == 1 && foreground.get_blue() == 1)
	pText->property_foreground() = colorDark;

    pText->property_wrap_width() = WRAP_SIZE_NAME_COL;
    pText->property_wrap_mode() = Pango::WRAP_CHAR;
}

void
Trees::on_rowActivated(const Gtk::TreeModel::Path& path,
    Gtk::TreeViewColumn* /* column */)
{
    Gtk::TreeModel::iterator iter = m_model->getRefTreeModelSort()->get_iter(path);

    if(iter)
    {
	Gtk::TreeModel::Row row = *iter;
	/* ObjectWindow *rw = */ new ObjectWindow(row, m_model);
    }
}
