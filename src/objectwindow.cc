/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2019-2024 Alfonso Sabato Siciliano
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <gtkmm/messagedialog.h>
#include <gtkmm/adjustment.h>
#include <gtkmm/button.h>
#include <gtkmm/grid.h>
#include <gtkmm/label.h>
#include <gtkmm/textview.h>
#include <gtkmm/textbuffer.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/viewport.h>

#include "objectwindow.h"
#include "sysctlexception.h"

ObjectWindow::ObjectWindow(Gtk::TreeModel::Row row, Model *model)
{
    m_row=row;
    m_model=model;

    int i;
    bool isFirst=true;
    Glib::ustring prop(""), value(""), flagsValue(""), handler("");
    Glib::RefPtr<Gtk::Adjustment> h = Gtk::Adjustment::create(0,0,100);
    Glib::RefPtr<Gtk::Adjustment> w = Gtk::Adjustment::create(0,0,100);
    Gtk::Viewport *viewport = new Gtk::Viewport(h,w);
    Gtk::Grid *grid = Gtk::manage( new Gtk::Grid() );
    Gtk::Label *tmpLabel;
    Glib::RefPtr<Gtk::TextBuffer> tmpBuffer;
    Gtk::TextView *tmpTextView;
    Gtk::ScrolledWindow *scrolledWindow = Gtk::manage(new Gtk::ScrolledWindow());
    Gtk::Button *button = Gtk::manage(new Gtk::Button("Set Value"));
    m_newValueEntry = Gtk::manage(new Gtk::Entry());

    struct field {
	const char *name;
	Glib::ustring value;
    };

    // Set this Window
    set_default_size(500, 400);
    set_resizable(true);
    set_title(row[model->m_col_name]);
    set_border_width(10);

    // build the widgets
    grid->set_row_spacing(5);
    grid->set_column_spacing(8);

    for(i=0; i<NUM_COLSFLAGS; i++) {
	if(row[*(model->flagTreeCols[i].modelColumn)]) {
	    if(!isFirst)
	     flagsValue += "\n";
	    isFirst = false;
	    flagsValue += model->flagTreeCols[i].flag_name;
	    flagsValue += ",  "; 
	    flagsValue += model->flagTreeCols[i].flag_desc;
	}
    }

    value = model->getSysctlValue(row[model->m_col_completename], true, -1, "ERROR");
    if(value.size() <= 0){
	value = "";
    }
    
    try {
	handler = m_model->hasHandler(row[model->m_col_completename]) ?
	    "Defined" : "Undefined";
    }
    catch (SysctlException *e) {
	handler = "ERROR: ";
	handler += e->what();
    }


    struct field fields[9] = {
	{"OID", row[model->m_col_oid] },
	{"Name", row[model->m_col_completename] },
	{"Description", row[model->m_col_desc] },
	{"Label", row[model->m_col_label] },
	{"Flags", flagsValue },
	{"Handler", handler },
	{"Type", row[model->m_col_type] },
	{"Format", row[model->m_col_fmt] },
	{"Value", value }
    };


    for(i=0; i < 9; i++) {
	tmpLabel = Gtk::manage( new Gtk::Label(fields[i].name) );
	tmpLabel->set_alignment(1,0);
	grid->attach(*tmpLabel, 0, i, 1, 1);
	
	tmpBuffer = Gtk::TextBuffer::create();
	if(i == 8)
	    m_valueBuffer=tmpBuffer;
	tmpBuffer->set_text(fields[i].value);
	tmpTextView = Gtk::manage( new Gtk::TextView());
	tmpTextView->set_buffer(tmpBuffer);
	if(i == 8) // value
	    tmpTextView->set_monospace(true);
	tmpTextView->set_hexpand(true);
	tmpTextView->set_left_margin(3);
	tmpTextView->set_right_margin(3);
	//if(i != 8) // value
	    tmpTextView->set_wrap_mode(Gtk::WRAP_WORD_CHAR);
	tmpTextView->set_editable(false);
	tmpTextView->set_cursor_visible(false);
	grid->attach(*tmpTextView, 1, i, 1, 1);
    }

    // new Value row
    grid->attach(*button, 0, i, 1, 1);
    button->signal_clicked().connect(
	sigc::mem_fun(*this, &ObjectWindow::on_button_clicked));

    m_newValueEntry->set_hexpand(true);
    grid->attach(*m_newValueEntry, 1, i, 1, 1);

    if(!row[model->m_col_WR]) {
	button->set_sensitive(false);
	m_newValueEntry->set_text("Not Writable");
	m_newValueEntry->set_sensitive(false);
    }


    scrolledWindow->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
    scrolledWindow->set_propagate_natural_width(true);
    scrolledWindow->set_propagate_natural_height(true);
    viewport->add(*grid);
    scrolledWindow->add(*viewport);
    add(*scrolledWindow);
    show_all_children();
    show();

}

void ObjectWindow::on_button_clicked()
{
    Gtk::MessageDialog sysctlOutput(*this, "SET VALUE", false, Gtk::MESSAGE_INFO);
    Glib::ustring text2("");

    sysctlOutput.set_title(m_row[m_model->m_col_name]);

    text2 = "[OLD Value]: ";
    text2 += m_valueBuffer->get_text();
    text2 += "\n";
    try {
	m_valueBuffer->set_text(m_model->setSysctlValue(m_row, m_newValueEntry->get_text()));
	text2 += "[NEW Value]: ";
	text2 += m_valueBuffer->get_text();
	text2 += "\n";
	m_newValueEntry->set_text("");
    }
    catch (SysctlException *e) {
	sysctlOutput.property_message_type() = Gtk::MESSAGE_ERROR;
	sysctlOutput.set_message("Error");
	text2 = Glib::ustring(e->what());
    }

    sysctlOutput.set_secondary_text(text2);
    sysctlOutput.run();
}
