/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2019-2024 Alfonso Sabato Siciliano
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _SYSCTLVIEW_MODEL_H_
#define _SYSCTLVIEW_MODEL_H_

#include <sys/sysctl.h>

extern "C"{
#include <sysctlmibinfo2.h>
}

#include <gtkmm/treeview.h>
#include <gtkmm/treestore.h>
#include <gtkmm/treemodelfilter.h>
#include <gtkmm/treemodelsort.h>

#define NUM_COLSMAIN 7
struct mainTreeCol {
    const char *infoName;
    Gtk::TreeModelColumn<Glib::ustring> *modelColumn;
    bool visible; // visible on startup
};

#ifndef CTLFLAG_NEEDGIANT
#define CTLFLAG_NEEDGIANT 0x00000800
#endif
#define NUM_COLSFLAGS 21
struct flagTreeCol {
    unsigned int flag_bit;
    const char *flag_name;
    const char *flag_desc;
    Gtk::TreeModelColumn<bool> *modelColumn;
    bool visible; // visible on startup
};

class Model
{

public:
    Model();
    virtual ~Model();

    void refreshValues();
    bool rebuildTrees();
    void toggleInternalRows();
    Glib::RefPtr<Gtk::TreeModelSort> getRefTreeModelSort();
    bool hasHandler(Glib::ustring name);
    Glib::ustring getSysctlValue(Glib::ustring name, bool withSkip,
	int longThreshold, Glib::ustring longstr);
    Glib::ustring setSysctlValue(Gtk::TreeModel::Row row, Glib::ustring newValue);

    // filter
    Gtk::TreeModelColumn<bool> m_col_isInternalRow;
    // both trees (adding manually)
    Gtk::TreeModelColumn<Glib::ustring> m_col_name;
    // mainTreeView
    Gtk::TreeModelColumn<Glib::ustring> m_col_oid;
    Gtk::TreeModelColumn<Glib::ustring> m_col_desc;
    Gtk::TreeModelColumn<Glib::ustring> m_col_label;
    Gtk::TreeModelColumn<Glib::ustring> m_col_type;
    Gtk::TreeModelColumn<Glib::ustring> m_col_fmt;
    Gtk::TreeModelColumn<Glib::ustring> m_col_value;
    Gtk::TreeModelColumn<Glib::ustring> m_col_flags;
    // no columns
    Gtk::TreeModelColumn<Glib::ustring> m_col_completename; // oidwindow
    Gtk::TreeModelColumn<unsigned int> m_col_uintflags;     // oidwindow
    Gtk::TreeModelColumn<unsigned int> m_col_uinttype;      // model setSysctlValue
    // flagsTreeView
    Gtk::TreeModelColumn<bool> m_col_RD;
    Gtk::TreeModelColumn<bool> m_col_WR;
    Gtk::TreeModelColumn<bool> m_col_RW;
    Gtk::TreeModelColumn<bool> m_col_DORMANT;
    Gtk::TreeModelColumn<bool> m_col_ANYBODY;
    Gtk::TreeModelColumn<bool> m_col_SECURE;
    Gtk::TreeModelColumn<bool> m_col_PRISON;
    Gtk::TreeModelColumn<bool> m_col_DYN;
    Gtk::TreeModelColumn<bool> m_col_SKIP;
    Gtk::TreeModelColumn<bool> m_col_CTLMASK_SECURE;
    Gtk::TreeModelColumn<bool> m_col_TUN;
    Gtk::TreeModelColumn<bool> m_col_RDTUN;
    Gtk::TreeModelColumn<bool> m_col_RWTUN;
    Gtk::TreeModelColumn<bool> m_col_MPSAFE;
    Gtk::TreeModelColumn<bool> m_col_NEEDGIANT;
    Gtk::TreeModelColumn<bool> m_col_VNET;
    Gtk::TreeModelColumn<bool> m_col_DYING;
    Gtk::TreeModelColumn<bool> m_col_CAPRD;
    Gtk::TreeModelColumn<bool> m_col_CAPWR;
    Gtk::TreeModelColumn<bool> m_col_STATS;
    Gtk::TreeModelColumn<bool> m_col_NOFETCH;
    Gtk::TreeModelColumn<bool> m_col_CAPRW;  
	
    struct mainTreeCol mainTreeCols[NUM_COLSMAIN] = {
	{ "OID",         &m_col_oid,   false},
	{ "Label",       &m_col_label, false},
	{ "Description", &m_col_desc,  true },
	{ "Flags",       &m_col_flags, false},
	{ "Type",        &m_col_type,  false},
	{ "Format",      &m_col_fmt,   false},
	{ "Value",       &m_col_value, true }
    };

    struct flagTreeCol flagTreeCols[NUM_COLSFLAGS] = {
	{ CTLFLAG_RD,       "RD",      "Can read the value",
	    &m_col_RD, true},
	{ CTLFLAG_WR,       "WR",      "Can write the value",
	    &m_col_WR, true },
	{ CTLFLAG_RW,       "RW",      "RD and WR",
	    &m_col_RW, false},
	{ CTLFLAG_ANYBODY,  "ANYBODY", "All users can set this var",
	    &m_col_ANYBODY, true},
	{ CTLFLAG_TUN,      "TUN",     "Default value is loaded from getenv()",
	    &m_col_TUN, true },
	{ CTLFLAG_RDTUN,    "RDTUN",   "RD and TUN",
	    &m_col_RDTUN, false},
	{ CTLFLAG_RWTUN,    "RWTUN",   "RW and TUN",
	    &m_col_RWTUN, false},
	{ CTLFLAG_STATS,    "STATS",   "Statistics, not a tuneable",
	    &m_col_STATS, true},
	{ CTLFLAG_NOFETCH,  "NOFETCH", "Don't fetch tunable from getenv()",
	    &m_col_NOFETCH, true},
	{ CTLFLAG_CAPRD,    "CAPRD",   "Can read the value in Capability mode",
	    &m_col_CAPRD, true},
	{ CTLFLAG_CAPWR,    "CAPWR",   "Can write the value Capability mode",
	    &m_col_CAPWR, true},
	{ CTLFLAG_CAPRW,    "CAPRW",   "CAPRD and CAPWR",
	    &m_col_CAPRW, false},
	{ CTLFLAG_SECURE,   "SECURE",  "Can write the value only if securelevel<=0",
	    &m_col_SECURE, true},
	{ CTLFLAG_PRISON,   "PRISON",  "Prisoned roots can fiddle",
	    &m_col_PRISON, true},
	{ CTLFLAG_VNET,     "VNET",    "Prisons with vnet can fiddle",
	    &m_col_VNET, true},
	{ CTLFLAG_MPSAFE,   "MPSAFE",  "Multiprocessor safe",
	    &m_col_MPSAFE, true},
	{ CTLFLAG_NEEDGIANT,"NEEDGIANT", "Requires Giant lock",
	    &m_col_NEEDGIANT, false},
	{ CTLFLAG_DYN,      "DYN",     "Dynamic parameter/OID (can be freed)",
	    &m_col_DYN, true},
	{ CTLFLAG_DYING,    "DYING",   "Parameter is being removed (kernel debug)",
	    &m_col_DYING, false},
	{ CTLFLAG_DORMANT,  "DORMANT", "The parameter is not active yet (kernel debug)",
	    &m_col_DORMANT, false},
	{ CTLFLAG_SKIP,     "SKIP",    "Skip the value when listing",
	    &m_col_SKIP, false}
    };

private:

    // members:
    Gtk::TreeModel::ColumnRecord *m_Columns;
    Glib::RefPtr<Gtk::TreeStore> m_refTreeModel;
    bool m_flagShowInternalRow;
    Glib::RefPtr<Gtk::TreeModelFilter> m_refTreeModelFilter;
    Glib::RefPtr<Gtk::TreeModelSort> m_refTreeModelSort;
    int m_countValues;

    // methods:
    bool populateTreeModel();
    void preorderAddNode(struct sysctlmif_object *node, Gtk::TreeModel::Row *parent);
    bool refreshValueIter(const Gtk::TreeModel::iterator& iter);
    Glib::ustring getSysctlValue(struct sysctlmif_object *obj, bool withSkip,
	int longThreshold, Glib::ustring longOrSkipStr);
};

#endif // _SYSCTLVIEW_MODEL_H_
