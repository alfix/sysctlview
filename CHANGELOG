2021-03-12 version 2.1

	* update: sysctl library to sysctlmibinfo2 2.0.1
	* update: menu link "Help -> Handbook"
	* update: handle NEEDGIANT like a normal flag
	* update: menu "Help -> Flags"
	* update: manual DESCRIPTION
	* update: menu "Help -> FAQ"
	* add: sort trees clicking column title
	* add: "[CLICK TO SHOW]" for value of a SKIP object in the Main tree
	* add: Handler entry in ObjectWindow
	* add: opaque format S,pagesizes
	* fix: indent menu "Help -> About -> License"
	* fix: delete CTLMASK_SECURE
	* delete: license.h, merged with mainwindow.cc

2020-06-12 version 2.0.1

	* fix: "Name column" color depending on the theme
	* fix: Inactive "Set Value" button/entry if the object has not CTLFLAG_WR
	* fix: unused vars (adding -Wall to Makefile)

2020-02-23 version 2.0

	* add: setting value feature ("New Value" button in Object Window)
	* add: menu "Help -> Handbook", using xdg-open <devel/xdg-utils>
	* add: NEEDGIANT column flag for the new flag in sysctl.h, this flag
		does not add information, it is just the complement of the
		MPSAFE flag, therefore it is retrieved via "not MPSAFE"
		so sysctlview has not to handle the old kernel versions
	* update: menu "View -> Magical Nodes " -> "View Internal Nodes"
	* update: manual sysctlview.1

2019-11-26 version 1.6

	* delete: the support for the kernel sysctl undocumented interface,
		support only the sysctlinfo interface via sysctlmibinfo2
	* update: dependence sysctlmibinfo.so -> sysctlmibinfo2.so
	* update: rename menu "View->Main Tree->ID" -> "View->Main Tree->OID"
	* update: rename OIDWindow -> ObjectWindow
	* update: rename "ID" -> "OID" in objectwindow.cc
	* fix: ObjectWindow value in monospace to indent like sysctl(8)

2019-10-05 version 1.5.2

	* delete: CTL_SYSCTLMIB, CTL_SYSCTL added in sysctl.h
	* add: an error dialog if an OID has CTL_MAXNAME - 2 levels and the
		sysctlinfo is not loaded (avoiding a segmentation fault)

2019-09-12 version 1.5.1

	* fix: "View -> Magical nodes" when the sysctlinfo module is loaded

2019-09-07 version 1.5

	* add: 'sysctlinfo' kernel interface support:
	* add: a warning dialog if the sysctlinfo kmod is not loaded
	* fix: handle nodes up to CTL_MAXNAME levels
	* fix: show the right value of the nodes without the last name
	* fix: show subtrees without leaves (all the node are CTLTYPE_NODE)
	* improve: efficiency, sysctlinfo is 30% more efficient than the
		kernel undocumented interface

2019-06-25 version 1.4

	* improve: searching
	* change: dafault 'Flags' columns
	* change: searchEntry size
	* change: "[VALUE TOO LONG, CLICK TO SHOW]" -> "[CLICK TO SHOW]"
	* add: menu item "View -> Main tree -> All"
	* add: menu item "View -> Main tree -> Defaults"
	* add: menu item "View -> Flags tree -> All"
	* add: menu item "View -> Flags tree -> Defaults"
	* add: support for S,bios_smap_xattr opaque value (e.g., machdep.smap)

2019-06-11 version 1.3

	* fix: memory leak OIDWindow
	* fix: columns size after "Rebuild all"
	* fix: AND flags
	* change: OIDWindow layout
	* add: ProgressBar (progressbar.h/cc)
	* add: 'ID' column
	* add: menu item "View -> Search Name"
	* add: menu item "View -> Wrap Rows"


2019-05-28 version 1.2

	* fix: Kelvin ("IK*" format) value
	* fix: length of a column (set wrap property)
	* change: RowWindow -> OIDWindow
	* add: Model class (model.h/cc)
	* add: menu item "File -> refresh values"
	* add: menu item "File -> rebuild the tree"
	* add: menu "View"
	* add: menu item "View -> exapand all nodes"
	* add: menu item "View -> collapse the tree"
	* add: menu item "View -> show magical rows"
	* add: menu items "View -> Main tree -> 'info' column"
	* add: menu items "View -> Flags tree -> 'flag' column"
	* add: menu item "Help -> FAQ"
	* add: support for S,clockinfo opaque value (e.g., kern.clockrate)
	* add: support for S,input_id opaque value (e.g., kern.evdev.input.0.id)
	* add: support for S,loadavg opaque value (e.g., vm.loadavg)
	* add: support for S,timeval opaque value (e.g., kern.boottime)


2019-05-22 version 1.1

	* add: copyright to "aboutDialog"
	* add: logo to "aboutDialog"
	* add: icon to "MyWindow"
	* fix: 'PR237159' build with GCC-based architectures
	* fix: font color of the 'name' column depending on the theme
	* fix: valuelen with  "double sysctl() call" (e.g., hw.dri.0.vblank)
	* delete: footer label (copyright)


2019-03-29 version 1.0

	* add: "LONG VALUE, CLICK TO SHOW" (e.g., kern.conftxt)
	* add: icons
	* update: "aboutDialog"
	* update: show a Window when a row is clicked, no a Dialog
	* update: 'resizable' columns in "Main" TreeView
	* update: 'resizable' "name" column in "Flags" TreeView
	* fix: numeric array (e.g., kern.cp_times)
	* fix: don't get the value of a CTLTYPE_NODE
	* fix: unicode values (e.g., kern.cam.ada.0)
	* fix: clean code


2019-03-23 version 0.2

	* change: C GTK -> C++ GTKmm
	* add: menu
	* add: "node dialog" when a row is clicked
	* add: manual page "sysctlview.1"
	* add: TreeView "Main"
	* delete: TreeView "description"
	* delete: TreeView "values"
	* delete: TreeView "info"
	* delete: sysctlmibinfo.c/h (dynamic linking)
	* delete: sysctlview.window.xml


2019-02-02 version 0.1

	* add: TreeView "description"
	* add: TreeView "values"
	* add: TreeView "info"
	* add: TreeView "flags"
