# Public Domain, NO WARRANTY, see:
#     <http://creativecommons.org/publicdomain/zero/1.0/>
#
# Written by Alfonso Sabato Siciliano

OUTPUT = sysctlview
SRC = ${.CURDIR}/src

RM= rm -f
LN = ln -s -f

all : ${OUTPUT}

${OUTPUT}:
	${MAKE} -C ${SRC}
	${LN} ${SRC}/${OUTPUT} ${.CURDIR}/${OUTPUT}

clean:
	${MAKE} -C ${SRC} clean
	${RM} ${OUTPUT} *.core


